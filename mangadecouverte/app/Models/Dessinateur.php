<?php

namespace App\Models;
use DB;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Dessinateur extends Model
{
    use HasFactory;

    public function getAll()
    {
        //code
        $dessinateurs = DB::table('dessinateur')->get();
        return $dessinateurs;
    }


}
